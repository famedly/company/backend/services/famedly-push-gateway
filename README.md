# Famedly-push-gateway
Dead simple push gateway for matrix push notifications to Firebase Cloud Messaging.

### First steps
Clone the repo and Install dependencies:
```
git clone https://gitlab.com/famedly/libraries/famedly-push-gateway.git
cd famedly-push-gateway
npm install
```

Edit the config.json and add the path to a valid ssl cert and add your Firebase admin key.

Run it with
```
node famedlyPushGateway.js
```

### Run it in background
Use `forever`:

```
sudo npm install forever -g
forever start famedlyPushGateway.js
```