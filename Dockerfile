FROM alpine
RUN apk add npm

COPY ./famedlyPushGateway.js /opt/pushgateway/
COPY ./package.json /opt/pushgateway/
COPY ./package-lock.json /opt/pushgateway/

RUN ["/usr/bin/npm", "install", "/opt/pushgateway/"]
ENTRYPOINT ["/usr/bin/node", "/opt/pushgateway/famedlyPushGateway.js"]
