const app = require("http");
const request = require('request');
const config = require('./config.json');
const default_notification = "com.famedly.default_notification";

app.createServer(responde).listen(config.port);

function getDateString() {
    const date = new Date();
    return date.getFullYear() +
        "-" + ("0" + (date.getMonth() + 1)).slice(-2) +
        "-" + ("0" + date.getDate()).slice(-2) +
        " " + ("0" + date.getHours()).slice(-2) +
        ":" + ("0" + date.getMinutes()).slice(-2);
}

function responde(req, res) {
    if (req.method !== 'POST') {
        res.writeHead(405, { "Content-Type": "application/json" });
        res.write('{"errcode":"M_UNRECOGNIZED","error":"Unrecognized request"}');
        res.end();
        return;
    }
    var jsonString = '';

    var onData = function (data) {
        jsonString += data;
    }

    req.on('data', onData);

    req.on('end', function () {
        try {
            notification = JSON.parse(jsonString).notification;

            var devices = notification.devices;
            var rejected = [];
            var registrationIds = [];
            for (var i = 0; i < devices.length; i++) {
                if (devices[i].app_id === "com.famedly.app")
                    registrationIds[registrationIds.length] = devices[i].pushkey;
            }
            if (registrationIds.length === 0) throw ("No pushkey found!");

            var isMessage = typeof notification["event_id"] === "string" && notification["event_id"] !== "";

            var unread = typeof notification["counts"] === "object" &&
                typeof notification["counts"]["unread"] === "number" ?
                notification["counts"]["unread"] :
                0;

            var title = unread < 2 ? "Neue Nachricht" : (unread + " ungelesene Unterhaltungen");

            notification["click_action"] = "FLUTTER_NOTIFICATION_CLICK";

            var notifydata = {
                "collapse_key": default_notification,
                "badge": "" + (unread || "0"),
                "data": notification
            };
            if (registrationIds.length === 1)
                notifydata["to"] = registrationIds[0];
            else
                notifydata["registration_ids"] = registrationIds;
            if (isMessage) {
                notifydata["notification"] = {
                    "title": title,
                    "body": "App öffnen, um Nachricht zu entschlüsseln",
                    "badge": "" + unread,
                    "sound": "default",
                    "icon": "notifications_icon",
                    "tag": default_notification,
                    "android_channel_id": "com.famedly.app.message"
                };
            }
            request.post("https://fcm.googleapis.com/fcm/send", {
                json: notifydata,
                headers: {
                    "Authorization": "key=" + config.adminKey,
                    "Content-Type": "application/json"
                }
            }, function (err, result, body) {
               if (err) {
                    console.error("POST https://fcm.googleapis.com/fcm/send failed:", err);
               }
               console.log('POST HTTP status-code: ', result && result.statusCode);
               try {
                    console.log(getDateString() + " New Notification received:");
                    console.log(notification);
                    console.log(getDateString() + " Push Gateway sending:");
                    console.log(notifydata);
                    console.log(getDateString() + " Firebase response:");
                    console.log(body);

                    if (body && body.results) {
                        for (var i = 0; i < body.results.length; i++) {
                            if (typeof body.results[i].error === "string" && body.results[i].error !== "") {
                                if (devices.length === 1) {
                                    rejected.push(devices[0].pushkey);
                                }
                                else if (typeof body.results[i].registration_id === "string" && body.results[i].registration_id !== "") {
                                    rejected.push(body.results[i].registration_id);
                                }

                            }
                        }
                    }

                    console.log(getDateString() + " Response to Matrix server:");
                    console.log(rejected);
                    res.writeHead(200, { "Content-Type": "application/json" });
                    res.write('{"rejected": ' + JSON.stringify(rejected) + '}');
                    res.end();
                }
                catch (e) {
                    console.log(getDateString() + " Exception:");
                    console.error(e);
                    res.writeHead(400, { "Content-Type": "application/json" });
                    res.write('{"errcode":"M_UNKNOWN","error":"' + e + '"}');
                    res.end();
                    return;
                }
            }
            );
        }
        catch (e) {
            console.log(getDateString() + " Exception:");
            console.error(e);
            res.writeHead(400, { "Content-Type": "application/json" });
            res.write('{"errcode":"M_UNKNOWN","error":"' + e + '"}');
            res.end();
            return;
        }
    }
    );
}
